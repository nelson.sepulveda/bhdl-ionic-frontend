import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

import { APIServices } from '../services/services';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    constructor(
        private router: Router,
        private http: APIServices,
        private menu: MenuController,
        public toastController: ToastController,
        public loading: LoadingController
    ) { }

    private accountsProducts: any;
    private creditCarts: any;
    private userData: object = {};
    private showMenu: boolean = true;

    ngOnInit() {
        this.init()
    }

    async init () {
        this.loadGetUserData()
        this.loadProductsAccounts()
        this.loadCreditCart()
    }

    async loadProductsAccounts () {
        this.http.getProductoAccounts().subscribe(
            (data) => this._successProductAccount(data),
            (error) => this._errorProductAccount(error)
        )
    }

    async loadGetUserData () {
        this.http.getDataUser().subscribe(
            (data)=> this._dataSuccess(data),
            (error) => this._dataError(error)
        )
    }

    async loadCreditCart () {
        this.http.getProductCreditCart().subscribe(
            (data) => this._successCreditCart(data),
            (error) => this._errorCreditCart(error)
        )
    }

    getNumberMount (number: number) {
        let dollarUSLocale = Intl.NumberFormat('en-US');
        return dollarUSLocale.format(number)
    }

    getNumberAccount(numero: string) {
        return '*' + numero.substr(numero.length-4, numero.length)
    }

    _successCreditCart (data: any) {
        this.creditCarts = data
    }

    async _errorCreditCart (error: any) {
        console.log(error)
        const toast = await this.toastController.create({
            message: error.error.message || 'Error en carga de tus productos',
            position: 'top',
            cssClass: 'ion-toast-login__custom',
            duration: 3500
        });
        toast.present();
    }

    _successProductAccount (data: any) {
        this.accountsProducts = data
    }

    async _errorProductAccount (error: any) {
        console.log(error)
        const toast = await this.toastController.create({
            message: error.error.message || 'Error en carga de tus productos',
            position: 'top',
            cssClass: 'ion-toast-login__custom',
            duration: 3500
        });
        toast.present();
    }

    _dataSuccess (data: any) {
        this.userData = { ... data }
    }

    _dataError (error: any) {
        console.log(error)        
    }

    closeMenuDashboard () {
        this.showMenu = true
    }

    openMenuDashboard () {
        this.menu.enable(true, 'start');
        this.menu.open('start');
        this.showMenu = false
    }

    async cerrarSesion () {
        const loading = await this.loading.create({
            cssClass: 'my-custom-class',
            spinner: 'circular',
            duration: 1500
        });
        await loading.present();
        if (localStorage.getItem('tokens')) {
            localStorage.removeItem('tokens')
            this.router.navigateByUrl('/home')
        }
    }
}
