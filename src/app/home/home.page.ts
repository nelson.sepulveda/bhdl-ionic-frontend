import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ModalController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

import { APIServices } from '../services/services';
import { SlidesComponent } from '../slides/slides.component';
@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})

export class HomePage {

    constructor(
        private http: APIServices,
        private menu: MenuController,
        public toastController: ToastController,
        public Loading: LoadingController,
        public Modal: ModalController,
        private router: Router
    ) {}

    showMenu: boolean = true
    documento: string = ''
    contrasena: string = ''
    spinner: any

    ngOnInit() {
        this.init()
    }

    async init () {
        this.spinner = await this.Loading.create({
            spinner: 'circular'
        })
    }

    closeMenu () {
        this.showMenu = true
    }

    async openSlides () {
        const modal = await this.Modal.create({
            component: SlidesComponent,
            cssClass: 'my-custom-class'
        });
        return await modal.present();
    }

    validate () {
        if (!this.documento) {
            return {
                error: true,
                message: 'Tu documento es requerido'
            }
        } else if (!this.contrasena) {
            return {
                error: true,
                message: 'Tu contraseña es requerida'
            }
        }
        return {
            error: false,
            message: ''
        }
    }

    async actionLogin () {
        await this.spinner.present();
        const { error, message } = this.validate()
        if (error) {
            await this.spinner.dismiss();
            const toast = await this.toastController.create({
                message: message,
                position: 'top',
                cssClass: 'ion-toast-login__custom',
                duration: 2500
            });
            toast.present();
        } else {
            await this.spinner.dismiss();
            this.http.getLogin(this.documento, this.contrasena).subscribe(
                (data)=> this._loginSuccess(data),
                (error) => this._loginError(error)
            )
        }
        this.init()
    }

    async _loginSuccess (data: any) {
        console.log(data)
        localStorage.setItem('tokens', JSON.stringify(data))
        this.documento = ''
        this.contrasena = ''
        // await this.storage.set('tokens', JSON.stringify(data))
        this.router.navigateByUrl('/dashboard')
    }

    async _loginError (error: any) {
        console.log(error)
        let msg = 'Tenemos problemas tecnicos intenta mas tarde'
        if (error.status === 404 || error.status === 400 || error.status === 500) {
            msg = error.error.message
        }
        const toast = await this.toastController.create({
            message: msg,
            position: 'top',
            duration: 3500
            });
        toast.present();
    }

    openMenu() {
        this.menu.enable(true, 'start');
        this.menu.open('start');
        this.showMenu = false
    }

}
