# install dependencies
$ npm run install

# serve with hot reload at localhost:8100
$ ionic serve

# build for production and launch server
$ ionic build
