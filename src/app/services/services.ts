import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

export class APIServices {
    constructor(
        private http: HttpClient
    ) {}

    getLogin(documento: string, contrasena: string) {
        const data = {
            userId: documento,
            password: contrasena
        }
        return this.http.post(environment.apiUrl + environment.uris.sign_in, data)
    }

    getDataUser() {
        const tokens = JSON.parse(localStorage.getItem('tokens'))
        const headers = {
            authorization: `Bearer ${tokens.access_token}`
        }
        return this.http.get(environment.apiUrl + environment.uris.user_data, { headers })
    }

    getProductoAccounts() {
        const tokens = JSON.parse(localStorage.getItem('tokens'))
        const headers = {
            authorization: `Bearer ${tokens.access_token}`
        }
        return this.http.get(environment.apiUrl + environment.uris.products_accounts, { headers })
    }

    getProductCreditCart () {
        const tokens = JSON.parse(localStorage.getItem('tokens'))
        const headers = {
            authorization: `Bearer ${tokens.access_token}`
        }
        return this.http.get(environment.apiUrl + environment.uris.products_credit_cards, { headers })
    }
}